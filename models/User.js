// Import Mongoose
const mongoose = require("mongoose");

// Create user schema
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "Firstname is required"]
	},
	lastName: {
		type: String,
		required: [true, "Lastname is required"]
	},
	email: {
		type: String,
		required: [true, "email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	
})

// Exports User schema
module.exports = mongoose.model("User",userSchema);