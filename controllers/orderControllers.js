// Import order model
const Order = require("../models/Order");
// Import product model
const Product = require("../models/Product");
// Import user model
const User = require("../models/User");
// import auth
const auth = require("../auth");



// Non-admin User check out/ Create Order

module.exports.createOrder = (req,res) => {

	if (req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	}
	let id = req.user.id;

	let newOrder = new Order ({
		userId: id,
		totalAmount: req.body.totalAmount,
		products: req.body.products
	})

	newOrder.save()
	.then(result => res.send("Order Succesfully checkout!"))
	.catch(error => res.send(error))

}

// Retrieve authenticated user's orders
module.exports.retrieveUserOrders = (req,res) => {
	console.log(req.params);
	Order.find({id:req.params.userId})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

// Retrieve All orders by Admin only
module.exports.retrieveAllOrders = (req,res) => {
	Order.find()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


