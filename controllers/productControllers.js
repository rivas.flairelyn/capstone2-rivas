// Import user model
const Product = require("../models/Product");
// import auth
const auth = require("../auth");

// Create Product
module.exports.createProduct = (req,res) => {

	let newProduct = new Product ({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Retrieve all active Products
module.exports.retrieveAllActiveProducts = (req,res) => {

	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Retrieve single product
module.exports.retrieveSingleProduct = (req,res) => {

	Product.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

// Update Product information
module.exports.updateProduct = (req,res) => {

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

// Archive Product
module.exports.archiveProduct = (req,res) => {

	let update = {
		isActive: false
	}
	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

/*Strech Goal 1*/

// Activate Product
module.exports.activateProduct = (req,res) => {

	let update = {
		isActive: true
	}
	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


