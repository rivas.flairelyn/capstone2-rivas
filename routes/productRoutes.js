/*IMPORTS*/
// Import Express
const express = require("express");
// Import router
const router = express.Router();
// Import Models
const Product = require("../models/Product");
// Import Contollers
const productControllers = require("../controllers/productControllers");
// Import auth
const auth = require("../auth");
// destrtuctive auth
const {verify,adminVerification} = auth;


// Create Product
router.post('/',verify,adminVerification,productControllers.createProduct);

// Retrieve all active Products
router.get('/activeProducts',productControllers.retrieveAllActiveProducts);

// Retrieve single product
router.get('/singleProduct/:productId',productControllers.retrieveSingleProduct);

// Update Product information
router.put('/updateProductInformation/:productId',verify,adminVerification,productControllers.updateProduct);

// Archive Product
router.delete('/archiveProduct/:productId',verify,adminVerification,productControllers.archiveProduct);

// Activate Product
router.put('/activateProduct/:productId',verify,adminVerification,productControllers.activateProduct)
module.exports = router;