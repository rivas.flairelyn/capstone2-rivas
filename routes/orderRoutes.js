/*IMPORTS*/
// Import Express
const express = require("express");
// Import router
const router = express.Router();
// Import Models
const Order = require("../models/Order");
// Import Contollers
const orderControllers = require("../controllers/orderControllers");
// Import auth
const auth = require("../auth");
// destrtuctive auth
const {verify,adminVerification} = auth;


// Non-admin User check out/ Create Order
router.post('/createOrder/:userId',verify,orderControllers.createOrder);

// Retrieve authenticated user's orders
router.get('/retrieveOrders/:userId',verify,orderControllers.retrieveUserOrders);

// Retrieve All orders by Admin only
router.get('/retrieveOrders',verify,adminVerification,orderControllers.retrieveAllOrders);

module.exports = router;