/*IMPORTS*/
// Import Express
const express = require("express");
// Import router
const router = express.Router();
// Import Models
const User = require("../models/User");
// Import Contollers
const userControllers = require("../controllers/userControllers");
// Import auth
const auth = require("../auth");
// destrtuctive auth
const {verify,adminVerification} = auth;



// User registration
router.post('/',userControllers.registerUser);

// User login 
router.post('/login',userControllers.loginUser);

// Admin update user's IsAdmin
router.put('/updateUser/:userId',verify,adminVerification,userControllers.updateUser);

// Get User Details
router.get('/userDetails',verify,userControllers.getUserDetails);

// Check email if existing
router.post('/checkEmailExists',userControllers.checkEmailExists);

module.exports = router;